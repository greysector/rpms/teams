%define _enable_debug_packages %{nil}
%define debug_package          %{nil}
%global _build_id_links alldebug

Summary: Chat-centered workspace in Office 365
Name: teams
Version: 1.5.00.23861
Release: 1
URL: https://www.microsoft.com/en-us/microsoft-teams/group-chat-software
Source0: https://packages.microsoft.com/yumrepos/ms-teams/teams-%{version}-1.x86_64.rpm
Patch0: teams-path.patch
License: Microsoft
BuildRequires: desktop-file-utils

%global __provides_exclude_from ^%{_libdir}/teams
%global __requires_exclude ^lib\(GLESv2\|ffmpeg\)\.so

%description
Microsoft Teams for Linux is your chat-centered workspace in Office 365.
Instantly access all your team’s content from a single place where messages,
files, people and tools live together.
This is a preview version, so bear with us while we do some fine-tuning.

ONE PLACE FOR YOUR TEAM’S FILES AND CONVERSATIONS:
- Work with your team’s documents on the go
- Move seamlessly across different team projects and topics
- Mention individual team members or your entire team to get attention

STAY CONNECTED WITH PRIVATE AND TEAM CHAT:
- Chat privately one-on-one or have instant group conversations
- See real-time chat history on your favorite device and continue conversations
  started elsewhere
- Chat with your entire team in dedicated channels

QUICKLY FIND WHAT YOU NEED:
- Quickly search through your chats and team conversations
- Find a contact through name or email address search

TAILOR YOUR WORKSPACE:
- Include content and capabilities you need every day
- Customize alerts to get notified when you get mentioned or get a message
- Save important conversations to quickly access them later

SECURITY TEAMS TRUST:
- Get the enterprise-level security and compliance you expect from Office 365
- Enhanced security with multi-factor authentication

This app requires appropriate commercial Office 365 subscription.

If you are not sure about your company’s subscription or the services you have
access to, please contact your IT department.

By downloading this app, you agree to the license and privacy terms (see
aka.ms/privacy).

To learn more, please visit aka.ms/microsoftteams

%prep
%setup -qcT
rpm2cpio %{S:0} | \
    cpio --extract --make-directories --no-absolute-filenames --preserve-modification-time
%patch 0 -p1

%build

%install
install -dm755 %{buildroot}{%{_bindir},%{_libdir},%{_datadir}/{applications,pixmaps}}
install -pm755 usr/bin/teams %{buildroot}%{_bindir}/teams
cp -pr usr/share/teams %{buildroot}%{_libdir}
install -pm644 usr/share/pixmaps/teams.png %{buildroot}%{_datadir}/pixmaps/
desktop-file-install \
  --dir %{buildroot}%{_datadir}/applications \
  usr/share/applications/teams.desktop

%files
%license %{_libdir}/teams/LICENSE
%license %{_libdir}/teams/LICENSES.chromium.html
%license %{_libdir}/teams/resources/ReadmeFirstTermsofUse.txt
%license %{_libdir}/teams/resources/ThirdPartyNotice.txt
%{_bindir}/teams
%dir %{_libdir}/teams
%{_libdir}/teams/chrome_100_percent.pak
%{_libdir}/teams/chrome_200_percent.pak
%{_libdir}/teams/chrome-sandbox
%{_libdir}/teams/icudtl.dat
%{_libdir}/teams/libEGL.so
%{_libdir}/teams/libffmpeg.so
%{_libdir}/teams/libGLESv2.so
%{_libdir}/teams/libvulkan.so
%{_libdir}/teams/locales
%dir %{_libdir}/teams/resources
%{_libdir}/teams/resources/app.asar
%{_libdir}/teams/resources/app.asar.unpacked
%{_libdir}/teams/resources/assets
%{_libdir}/teams/resources/locales
%{_libdir}/teams/resources/node_modules
%{_libdir}/teams/resources/tmp
%{_libdir}/teams/resources.pak
%{_libdir}/teams/snapshot_blob.bin
%{_libdir}/teams/swiftshader
%{_libdir}/teams/teams
%{_libdir}/teams/v8_context_snapshot.bin
%{_datadir}/applications/teams.desktop
%{_datadir}/pixmaps/teams.png

%changelog
* Tue Sep 27 2022 Dominik Mierzejewski <dominik@greysector.net> - 1.5.00.23861-1
- update to 1.5.00.23861
- update main URL

* Thu Apr 21 2022 Dominik Mierzejewski <dominik@greysector.net> - 1.5.00.10453-1
- update to 1.5.00.10453
- expand file list to avoid "File listed twice" rpmbuild warning

* Thu Oct 14 2021 Dominik Mierzejewski <rpm@greysector.net> 1.4.00.26453-1
- update to 1.4.00.26453

* Sat Jun 19 2021 Dominik Mierzejewski <rpm@greysector.net> 1.4.00.13653-1
- update to 1.4.00.13653

* Wed Mar 31 2021 Dominik Mierzejewski <rpm@greysector.net> 1.4.00.7556-1
- update to 1.4.00.7556
- drop obsolete patch

* Tue Mar 23 2021 Dominik Mierzejewski <rpm@greysector.net> 1.4.00.4855-2
- fix opening external meeting links

* Wed Mar 10 2021 Dominik Mierzejewski <rpm@greysector.net> 1.4.00.4855-1
- update to 1.4.00.4855

* Wed Dec 02 2020 Dominik Mierzejewski <rpm@greysector.net> 1.3.00.30857-1
- update to 1.3.00.30857
- fix build-id links conflict with skypeforlinux

* Wed Oct 14 2020 Dominik Mierzejewski <rpm@greysector.net> 1.3.00.25560-1
- update to 1.3.00.25560

* Tue Sep 29 2020 Dominik Mierzejewski <rpm@greysector.net> 1.3.00.16851-1
- update to 1.3.00.16851

* Wed Apr 08 2020 Dominik Mierzejewski <rpm@greysector.net> 1.3.00.5153-1
- initial repackaging of Microsoft's RPM
